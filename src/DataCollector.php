<?php

require __DIR__.'../../vendor/autoload.php';

class DataCollector
{
    /**
     * @var array
     */
    private $tree;

    /**
     * @var array
     */
    private $list;

    /**
     * @throws Exception
     */
    public function __construct()
    {
        $tree = file_get_contents("data/tree.json");
        $list = file_get_contents("data/list.json");

        if (!$tree || !$list ){ throw new Exception('expected files are not existing'); } else {
            $this->tree = json_decode($tree);
            $this->list = json_decode($list);

        }
    }

    final function prepareData(): void
    {
        $data = $this->connectData();

        $json = $this->parseToJson($data);

        dump($json);die;
    }

    private function connectData(): array
    {
        $solutionArray = [];

        foreach ($this->tree as $tree)
        {
           foreach ($this->list as $list)
           {
               if ($tree->id == $list->category_id)
               {
                   #I'm not sure what to do with array children in tree.json, should I process them also?
                   #In the result example exists children but it is empty so I also added him to my array lol
                   $solutionArray[] = ['id' => $tree->id, 'name' => $list->translations->pl_PL->name, 'children' => []];
                   break;
               }
           }
        }

        return $solutionArray;
    }

    private function parseToJson(array $array): string
    {
        $json = json_encode($array);

        return $json;
    }
}